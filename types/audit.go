package types

import (
	"github.com/olivere/elastic"
	"github.com/segmentio/kafka-go"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apiserver/pkg/apis/audit"
)

// LogMessage struct is used to construct a message to be sent to OMS
type LogMessage struct {
	audit.Event
	Cluster     string
	ReferObject string
}

type EventList struct {
	metav1.TypeMeta
	// +optional
	metav1.ListMeta

	Items []*LogMessage
}

// MessageHandler is used to wrap es and kafka client
type MessageHandler struct {
	Writer      *kafka.Writer
	ESClient    *elastic.Client
	ESIndexName string
}

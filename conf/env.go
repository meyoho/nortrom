package conf

import (
	log "github.com/sirupsen/logrus"
	"github.com/vrischmann/envconfig"
	"io/ioutil"
	"strings"
)

//EnvConf describe env config
var EnvConf struct {
	Kafka struct {
		Host       string `envconfig:"default=127.0.0.1:9092"`
		AuditTopic string `envconfig:"default=ALAUDA_AUDIT_TOPIC"`
		Auth 	   bool `envconfig:"default=true"`
	}
	Es struct {
		Host     string `envconfig:"default=127.0.0.1:9200"`
		Index    string `envconfig:"default=audit"`
		Username string `envconfig:"default=alaudaes"`
		Password string `envconfig:"default=es_password_1qaz2wsx"`
	}
	Log struct {
		ToStdout bool   `envconfig:"default=false"`
		Level    string `envconfig:"default=INFO"`
		Size     int    `envconfig:"default=50"`
	}
	Audit struct {
		SkipAuth    bool `envconfig:"default=false"`
		ResAggrTop  int  `envconfig:"default=100"`
		UserAggrTop int  `envconfig:"default=200"`
	}
}

//InitEnvConfig init envs
func InitEnvConfig() {
	var eSUsernameFile = "/etc/pass_es/username"
	var eSPasswordFile = "/etc/pass_es/password"
	if err := envconfig.Init(&EnvConf); err != nil {
		log.Warnf("warn=%s\n", err.Error())
	}
	if username, err := ioutil.ReadFile(eSUsernameFile); err == nil {
		EnvConf.Es.Username = string(strings.TrimRight(string(username), "\n"))
	}

	if password, err := ioutil.ReadFile(eSPasswordFile); err == nil {
		EnvConf.Es.Password = string(strings.TrimRight(string(password), "\n"))
	}
}

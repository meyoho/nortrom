package common

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/olivere/elastic"
	"github.com/olivere/elastic/config"
	"github.com/segmentio/kafka-go"
	"github.com/segmentio/kafka-go/sasl/plain"
	"io/ioutil"
	"nortrom/conf"
	"nortrom/types"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"k8s.io/apiserver/pkg/apis/audit"
)

// Handler is kafka handler
var Handler *types.MessageHandler

// GetReferObjectName gets object been processed
func GetReferObjectName(e *audit.Event) (string, error) {
	var response map[string]interface{}
	if e.Verb == "create" {
		if e.ResponseObject != nil {
			err := json.Unmarshal(e.ResponseObject.Raw, &response)
			if err != nil {
				return "", err
			}
			metaData := response["metadata"].(map[string]interface{})
			value, ok := metaData["name"]
			if ok {
				return value.(string), nil
			}
		}
		return " ", errors.New("no name found")
	}
	return e.ObjectRef.Name, nil
}

// GetCluster get cluster, TODO please modify this. !!!
func GetCluster(e *audit.Event) string {
	// start with "/api" is global
	if strings.HasPrefix(e.RequestURI, "/api") {
		return "global"
	} else if strings.HasPrefix(e.RequestURI, "/kubernetes") {
		uriList := strings.SplitN(e.RequestURI, "/", 4)
		return uriList[2]
	}
	return ""
}

//GetCurrentDateTimeString get current time
func GetCurrentDateTimeString() string {
	now := time.Now().UTC()
	formattedNow := fmt.Sprintf("%d-%02d-%02dT%02d:%02d:%02d", now.Year(), now.Month(), now.Day(), now.Hour(), now.Minute(), now.Second())
	return formattedNow
}

// CreateHandler create handler
func CreateHandler() {
	var kafkaUsernameFile = "/etc/pass_secret/KAFKA_USER"
	var kafkaPasswordFile = "/etc/pass_secret/KAFKA_PASSWORD"
	var kafkaUsername string
	var kafkaPassword string
	var kafkWriterConfig kafka.WriterConfig
	if username, err := ioutil.ReadFile(kafkaUsernameFile); err == nil {
		kafkaUsername = string(strings.TrimRight(string(username), "\n"))
	}

	if password, err := ioutil.ReadFile(kafkaPasswordFile); err == nil {
		kafkaPassword = string(strings.TrimRight(string(password), "\n"))
	}


	kafkaTopic := conf.EnvConf.Kafka.AuditTopic
	kafkaHosts := conf.EnvConf.Kafka.Host
	brokerList := strings.Split(kafkaHosts, ",")
	esURL := conf.EnvConf.Es.Host
	esUsername := conf.EnvConf.Es.Username
	esPassword := conf.EnvConf.Es.Password

	if conf.EnvConf.Kafka.Auth {
		kafkWriterConfig = kafka.WriterConfig{
			Dialer: &kafka.Dialer{
				SASLMechanism:   plain.Mechanism{
					Username: kafkaUsername,
					Password: kafkaPassword,
				},
			},
			Brokers:  brokerList,
			Topic:    kafkaTopic,
			Balancer: &kafka.LeastBytes{},
		}
	} else {
		kafkWriterConfig = kafka.WriterConfig{
			Brokers:  brokerList,
			Topic:    kafkaTopic,
			Balancer: &kafka.LeastBytes{},
		}
	}
	kafkaWriter := kafka.NewWriter(kafkWriterConfig)

	sniffed := false
	cfg := &config.Config{
		URL:      esURL,
		Username: esUsername,
		Password: esPassword,
		Sniff:    &sniffed,
	}
	esClient, err := elastic.NewClientFromConfig(cfg)
	if err != nil {
		log.Warnf("can't get es client info, please check es cluster for detail... %s", err.Error())
		time.Sleep(time.Duration(5*1000) * time.Millisecond)
		panic(err)
	}

	Handler = &types.MessageHandler{
		Writer:      kafkaWriter,
		ESClient:    esClient,
		ESIndexName: conf.EnvConf.Es.Index,
	}
}

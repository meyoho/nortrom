# Build stage
FROM index.alauda.cn/alaudaorg/alaudabase-alpine-go:1.13-alpine3.10 as builder

WORKDIR $GOPATH/src/nortrom
COPY . .

# ldflags "-s -w" strips binary
RUN go test $(go list ./... | grep -v /vendor/ | grep -v /template/|grep -v /build/) -cover \
 && CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o $GOPATH/src/nortrom/bin/nortrom

# Release stage
FROM index.alauda.cn/alaudaorg/alaudabase-alpine-go:1.13-alpine3.10
WORKDIR /nortrom
RUN apk add --update ca-certificates supervisor curl
COPY --from=builder /go/src/nortrom/bin/nortrom ./nortrom
COPY --from=builder /go/src/nortrom/conf/supervisord.conf /etc/supervisord.conf

EXPOSE 80
RUN mkdir /var/log/mathilde  && \
    mkdir -p /var/log/supervisor && \
    chmod +x /nortrom/nortrom 
CMD ["supervisord", "--nodaemon", "--configuration", "/etc/supervisord.conf"]
package main

import (
	"net/http"

	"nortrom/common"
	"nortrom/conf"
	"nortrom/handler"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"
)

func setupRouter() *gin.Engine {
	r := gin.New()
	r.Use(common.Logger(), gin.Recovery())
	// Simple group: audit
	audit := r.Group("/v1/kubernetes-audits")
	{
		audit.GET("/types", handler.AggregateAudits)
		audit.GET("", handler.GetAudits)
		audit.POST("", handler.ProcessAudit)
	}

	r.GET("/_ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pretty double Qing !")
	})
	r.GET("/metrics", gin.WrapH(promhttp.Handler()))

	return r
}

func main() {
	conf.InitEnvConfig()
	common.InitLogger()
	common.CreateHandler()
	r := setupRouter()
	log.Info("alauda audit starting...")
	r.Run(":80")
	common.Handler.Writer.Close()
}

package handler

import (
	"errors"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	v1 "k8s.io/api/authorization/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	certutil "k8s.io/client-go/util/cert"
)

// CanIDo return bool value if i can do something
func CanIDo(c *gin.Context, verb string, group string, resource string) bool {
	const (
		rootCAFile = "/var/run/secrets/kubernetes.io/serviceaccount/ca.crt"
		hosts      = "https://kubernetes.default.svc.cluster.local"
	)
	ssar := &v1.SelfSubjectAccessReview{
		Spec: v1.SelfSubjectAccessReviewSpec{
			ResourceAttributes: &v1.ResourceAttributes{
				Verb:     verb,
				Resource: resource,
				Group:    group,
			},
		},
	}

	tokenInfo := c.Request.Header.Get("Authorization")
	if tokenInfo == "" || strings.Index(tokenInfo, "Bearer ") == -1 {
		log.Warnf("please bring with your bearer token")
		return false
	}
	result := strings.SplitN(tokenInfo, "Bearer ", 2)
	token := result[1]
	tlsClientConfig := rest.TLSClientConfig{}

	if _, err := certutil.NewPool(rootCAFile); err != nil {
		log.Warnf("Expected to load root CA config from %s, but got err: %v", rootCAFile, err)
	} else {
		tlsClientConfig.CAFile = rootCAFile
	}

	k8sConfig := &rest.Config{
		Host:            hosts,
		TLSClientConfig: tlsClientConfig,
		BearerToken:     string(token),
	}
	// creates the clientSet
	clientSet, err := kubernetes.NewForConfig(k8sConfig)
	if err != nil {
		log.Warnf("failed to make k8s clientset %s", err)
		return false
	}
	response, err := clientSet.AuthorizationV1().SelfSubjectAccessReviews().Create(ssar)
	if err != nil {
		log.Warnf("failed to get auth info in k8s %s", err)
		return false
	}
	return response.Status.Allowed
}

// IfIMissed check if i missed to add some param
func ValidAuditQueryParam(c *gin.Context) error {
	var parseErr error
	startTimeString, ok := c.GetQuery("start_time")
	if !ok {
		return errors.New("start_time needed")
	}
	endTimeString, ok := c.GetQuery("end_time")
	if !ok {
		return errors.New("end_time needed")
	}
	pageString, ok := c.GetQuery("page")
	if !ok {
		return errors.New("page needed")
	}
	pageSizeString, ok := c.GetQuery("page_size")
	if !ok {
		return errors.New("page_size needed")
	}

	if _, parseErr = strconv.ParseFloat(startTimeString, 32); parseErr != nil {
		return errors.New("start_time format is wrong")
	}
	if _, parseErr = strconv.ParseFloat(endTimeString, 32); parseErr != nil {
		return errors.New("end_time format is wrong")
	}
	if _, parseErr = strconv.ParseInt(pageString, 10, 0); parseErr != nil {
		return errors.New("page format is wrong")
	}
	if _, parseErr = strconv.ParseInt(pageSizeString, 10, 0); parseErr != nil {
		return errors.New("page_size format is wrong")
	}
	return nil
}

func ValidAuditAggregationParam(c *gin.Context) error {
	var parseErr error
	startTimeString, ok := c.GetQuery("start_time")
	if !ok {
		return errors.New("start_time needed")
	}
	endTimeString, ok := c.GetQuery("end_time")
	if !ok {
		return errors.New("end_time needed")
	}
	_, valid := c.GetQuery("field")
	if !valid {
		return errors.New("field needed")
	}

	if _, parseErr = strconv.ParseFloat(startTimeString, 32); parseErr != nil {
		return errors.New("start_time format is wrong")
	}
	if _, parseErr = strconv.ParseFloat(endTimeString, 32); parseErr != nil {
		return errors.New("end_time format is wrong")
	}

	return nil
}

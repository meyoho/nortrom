package handler

import (
	"net/http"
	"nortrom/conf"
	"strconv"

	"nortrom/common"
	"nortrom/storage"
	"nortrom/types"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apiserver/pkg/apis/audit"
)

// AggregateAudits handle aggres handler
func AggregateAudits(c *gin.Context) {
	var startTime float64
	var endTime float64
	var topSize int
	fieldMapKey := map[string]string{
		"user_name": "User.Username", "resource_name": "ObjectRef.Resource"}
	err := ValidAuditAggregationParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"results": []string{err.Error()}})
		return
	}
	field := c.Query("field")
	if mappedValue, k := fieldMapKey[field]; k {
		field = mappedValue
	}
	startTime, _ = strconv.ParseFloat(c.Query("start_time"), 32)
	endTime, _ = strconv.ParseFloat(c.Query("end_time"), 32)
	indexes, _ := storage.GetIndexList(int64(startTime), int64(endTime), common.Handler)
	allowed := CanIDo(c, "get", "aiops.alauda.io", "audits")
	if allowed == false {
		c.JSON(http.StatusUnauthorized, gin.H{"results": []string{"auth check failed"}})
		return
	}
	if field == "User.Username" {
		topSize = conf.EnvConf.Audit.UserAggrTop
	} else if field == "ObjectRef.Resource" {
		topSize = conf.EnvConf.Audit.ResAggrTop
	} else {
		topSize = 10
	}
	fields, err := storage.GetAggregationFromES(startTime, endTime, field, topSize, indexes)
	if err != nil {
		c.JSON(http.StatusServiceUnavailable, gin.H{"result": fields})
	}

	c.JSON(http.StatusOK, gin.H{"result": fields})
}

// AuditProcess handle post response
func ProcessAudit(c *gin.Context) {
	var eventList audit.EventList
	allowed := CanIDo(c, "create", "aiops.alauda.io/v1beta1", "audits")
	if allowed == false && !conf.EnvConf.Audit.SkipAuth {
		c.JSON(http.StatusUnauthorized, gin.H{"results": []string{"auth check failed"}})
		return
	}
	ignoreKey := map[string]int{
		"get": 1, "watch": 1, "list": 1,
	}
	err := c.BindJSON(&eventList)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"results": []string{"unformat data"}})
		return
	}
	esAuditList := make([]*types.LogMessage, 0)
	for k := range eventList.Items {
		if _, k := ignoreKey[eventList.Items[k].Verb]; k {
			continue
		}
		// ignore this
		if eventList.Items[k].ObjectRef.Resource == "subjectaccessreviews" {
			continue
		}
		referObject, err := common.GetReferObjectName(&eventList.Items[k])
		cluster := common.GetCluster(&eventList.Items[k])
		esMessage := &types.LogMessage{
			eventList.Items[k],
			cluster,
			referObject,
		}
		esAuditList = append(esAuditList, esMessage)
		if err != nil {
			continue
		}
		log.Infof("%s %s %s ", eventList.Items[k].Verb, eventList.Items[k].ObjectRef.Resource, referObject)
	}
	err = storage.SendToKafka(esAuditList)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"results": []string{"send audit info failed, may you try again"}})
		return
	}
	c.JSON(http.StatusOK, gin.H{"results": []string{"send audit info succeed"}})
}

// GetAudits handle get request from gateway
func GetAudits(c *gin.Context) {
	var startTime float64
	var endTime float64
	var page int64
	var pageSize int64
	err := ValidAuditQueryParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"results": []string{err.Error()}})
		return
	}
	startTime, _ = strconv.ParseFloat(c.Query("start_time"), 32)
	endTime, _ = strconv.ParseFloat(c.Query("end_time"), 32)
	page, _ = strconv.ParseInt(c.Query("page"), 10, 0)
	pageSize, _ = strconv.ParseInt(c.Query("page_size"), 10, 0)

	indexes, _ := storage.GetIndexList(int64(startTime), int64(endTime), common.Handler)
	useName := c.Query("user_name")
	operationType := c.Query("operation_type")
	resourceName := c.Query("resource_name")
	resourceType := c.Query("resource_type")

	allowed := CanIDo(c, "get", "aiops.alauda.io", "audits")
	if allowed == false {
		c.JSON(http.StatusUnauthorized, gin.H{
			"results":     []string{},
			"total_pages": 0,
			"total_items": 0})
		return
	}

	logMessages, totalItems, totalPage := storage.GetAuditsFromES(startTime, endTime, indexes, operationType, resourceType,
		resourceName, useName, page, pageSize)
	typeMeta := metav1.TypeMeta{
		Kind:       "EventList",
		APIVersion: "audit.k8s.io/v1",
	}
	listMeta := metav1.ListMeta{}

	eventList := &types.EventList{
		typeMeta,
		listMeta,
		logMessages,
	}
	c.JSON(http.StatusOK, gin.H{"results": eventList,
		"total_pages": totalPage,
		"total_items": totalItems})
}

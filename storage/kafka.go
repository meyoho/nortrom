package storage

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/pkg/errors"
	"time"

	"nortrom/common"

	"github.com/segmentio/kafka-go"
	log "github.com/sirupsen/logrus"
)

func SendToKafka(data interface{}) error {
	requestBodyBytes := new(bytes.Buffer)
	json.NewEncoder(requestBodyBytes).Encode(data)
	requestBodyBytes.Bytes() // this is the []byte
	insertTime := time.Now()
	err := common.Handler.Writer.WriteMessages(context.Background(),
		kafka.Message{
			Key:   []byte(common.GetCurrentDateTimeString()),
			Value: requestBodyBytes.Bytes(),
		})
	if err != nil {
		log.Warnf("err happens when sending to kafka %s", err)
		return errors.New("errors happen send to kafka")
	} else {
		nowTime := time.Now()
		log.Infof("kafka success write time %f s", nowTime.Sub(insertTime).Seconds())
		return nil
	}
}

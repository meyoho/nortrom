package storage

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"nortrom/common"
	"nortrom/types"

	"github.com/olivere/elastic"
	log "github.com/sirupsen/logrus"
)

func GetAggregationFromES(startTime float64, endTime float64, field string, topSize int, indexes []string) ([]string, error) {
	var fields []string
	termAggregation := elastic.NewTermsAggregation()
	termAggregation = termAggregation.Field(field)
	termAggregation.Size(topSize)
	query := elastic.NewBoolQuery().Filter(elastic.NewRangeQuery("StageTimestamp").
		Gt(time.Unix(int64(startTime), 0).UTC().Format(time.RFC3339)).
		Lt(time.Unix(int64(endTime), 0).UTC().Format(time.RFC3339)))
	querySrc, _ := query.Source()
	data, _ := json.MarshalIndent(querySrc, "", "  ")
	log.Infof("query content is %s", string(data))
	searchResult, err := common.Handler.ESClient.
		Search().
		Query(query).
		Index(indexes...).                          // search in index "twitter"
		Aggregation("field_aggr", termAggregation). // specify the query
		Size(0).                                    // take documents 0-9
		Pretty(true).                               // pretty print request and response JSON
		Do(context.Background())                    // execute
	if err != nil {
		// Handle error
		log.Warnf("error happends when query es %s", err)
		return []string{}, err
	}

	termResults, found := searchResult.Aggregations.Terms("field_aggr")
	if found {
		for _, b := range termResults.Buckets {
			fields = append(fields, b.Key.(string))
		}
	}
	return fields, nil

}

func GetAuditsFromES(startTime float64, endTime float64, indexes []string, operationType string, resourceType string,
	resourceName string, useName string, page int64, pageSize int64) ([]*types.LogMessage, int64, int) {
	ignoreKey := map[string]int{
		"get": 1, "watch": 1, "list": 1,
	}
	rangeQuery := elastic.NewRangeQuery("StageTimestamp").
		Gt(time.Unix(int64(startTime), 0).UTC().Format(time.RFC3339)).
		Lt(time.Unix(int64(endTime), 0).UTC().Format(time.RFC3339))
	query := elastic.NewBoolQuery()
	if operationType != "" {
		operateTermQuery := elastic.NewTermQuery("Verb", operationType)
		query.Must(operateTermQuery)
	}
	if resourceType != "" {
		resourceTypeTermQuery := elastic.NewTermQuery("ObjectRef.Resource", resourceType)
		query.Must(resourceTypeTermQuery)
	}
	if resourceName != "" {
		resourceNameTermQuery := elastic.NewMatchQuery("ReferObject", resourceName)
		query.Must(resourceNameTermQuery)
	}
	if useName != "" {
		userTermQuery := elastic.NewTermQuery("User.Username", useName)
		query.Must(userTermQuery)
	}

	query.Filter(rangeQuery)
	querySrc, _ := query.Source()
	data, _ := json.MarshalIndent(querySrc, "", "  ")
	log.Infof("query content is %s", string(data))
	var from int
	if page > 1 {
		from = int(pageSize * (page - 1))
	} else {
		from = 0
	}

	searchResult, err := common.Handler.ESClient.
		Search().
		Query(query).
		Sort("StageTimestamp", false).
		Index(indexes...). // search in index "twitter"
		From(from).
		Size(int(pageSize)).     // take documents 0-9
		Pretty(true).            // pretty print request and response JSON
		Do(context.Background()) // execute
	if err != nil {
		log.Warnf("error happends when query es %s", err.Error())
		return []*types.LogMessage{}, 0, 0
	}

	if searchResult == nil || searchResult.TotalHits() == 0 {
		log.Warnf("get no search result")
		return []*types.LogMessage{}, 0, 0
	}
	var logMessages []*types.LogMessage
	for _, hit := range searchResult.Hits.Hits {
		logMessage := new(types.LogMessage)
		if err := json.Unmarshal(*hit.Source, logMessage); err != nil {
			log.Warnf("get search result unformated %s", err.Error())
			return []*types.LogMessage{}, 0, 0
		}
		if _, exsit := ignoreKey[logMessage.Verb]; !exsit {
			logMessages = append(logMessages, logMessage)
		}
	}
	totalItems := searchResult.TotalHits()
	totalPage := int((totalItems + pageSize - 1) / pageSize)
	return logMessages, totalItems, totalPage
}

// GetIndexList get index list from time range
func GetIndexList(startTimeSecond int64, endTimeSecond int64, handler *types.MessageHandler) ([]string, error) {
	if startTimeSecond >= endTimeSecond {
		return nil, errors.New("time format is wrong")
	}
	indexList := make([]string, 0)
	startTime := time.Unix(startTimeSecond, 0)
	endTime := time.Unix(endTimeSecond, 0)
	startDate := time.Date(startTime.Year(), startTime.Month(), startTime.Day(), 0, 0, 0, 0, time.UTC)
	endDate := time.Date(endTime.Year(), endTime.Month(), endTime.Day(), 0, 0, 0, 0, time.UTC)
	startDateString := fmt.Sprintf("%s-%s", handler.ESIndexName, startDate.Format("20060102"))
	exist, _ := handler.ESClient.IndexExists(startDateString).Do(context.Background())
	if exist {
		indexList = append(indexList, startDateString)
	}

	dayAlpha, _ := time.ParseDuration("24h")
	dayNext := startDate.Add(dayAlpha)

	for dayNext.Before(endDate) || dayNext.Equal(endDate) {
		startDate = dayNext
		nextDateString := fmt.Sprintf("%s-%s", handler.ESIndexName, dayNext.Format("20060102"))
		exists, _ := handler.ESClient.IndexExists(nextDateString).Do(context.Background())
		if exists {
			indexList = append(indexList, nextDateString)
		}
		dayNext = startDate.Add(dayAlpha)
	}
	return indexList, nil
}
